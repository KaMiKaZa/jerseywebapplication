<html>

<head>
    <title>Index page for my Jersey web service!</title>
</head>

<body>
    Something... in here?<br/>
    <ul>
        <a href="/jersey/somePath/testVariableForTest"><li>Go to somePath</li></a>
        <a href="/jersey/someGet?get=you got it or maybe not"><li>Go to someGet</li></a>
        <a href="/jersey/someCombined/testVariableForTest?get=incredible"><li>Go to someCombined</li></a>

        <li>
            <form action="/jersey/somePost" method="post">
                <input type="text" name="param1" />
                <input type="text" name="param2" />
                <input type="submit" text="GO!" />
            </form>
        </li>
    </ul>
</body>
</html>