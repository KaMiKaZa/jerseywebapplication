package com.kamikaza.jersey.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

// accept only pathVariable, returns plain string

@Path("somePath/{path}")
public class SomePath
{
    @GET
    @Produces("text/plain")
    public String pathMessage(@PathParam("path") String path)
    {
        String out = String.format("Jersey and Jetty wants to say: pathVariable is (%s)", path);

        return out;
    }

}
