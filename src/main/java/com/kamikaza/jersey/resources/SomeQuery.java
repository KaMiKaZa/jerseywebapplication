package com.kamikaza.jersey.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

// accept only GET param "get", returns plain string (but uses core.MediaType)

@Path("someGet")
public class SomeQuery
{
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getMessage(@QueryParam("get") String get)
    {
        String out = String.format("Jersey and Jetty wants to say: pathVariable is (%s)", get);

        return out;
    }
}
