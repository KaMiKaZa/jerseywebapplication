package com.kamikaza.jersey.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

// accept both PathVariable and GET variable, returns plain string

@Path("someCombined/{path}")
public class SomeCombined
{
    @GET
    @Produces("text/plain")
    public String combinedMessage(@PathParam("path") String path, @QueryParam("get") String get)
    {
        String out = String.format("Jersey and Jetty wants to say: pathVariable is (%s) and getVariable is (%s)", path, get);

        return out;
    }
}
