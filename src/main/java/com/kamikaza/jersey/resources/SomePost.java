package com.kamikaza.jersey.resources;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

// accept POST from index.jsp with fields "param1" and "param2"
// returns json (guess so)

@Path("somePost")
public class SomePost
{
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response somePost(@FormParam("param1") String param1, @FormParam("param2") String param2)
    {
        String json = String.format("{ \"param1\" : \"%s\", \"param2\" : \"%s\" }", param1, param2);

        // testing Response builds
        // HTTP code 200 == OK (successful)
        return Response.status(200).entity(json).build();
    }
}
