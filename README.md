# JAX-RS application with Jersey

For now has little amount of features: <br/>
map "/jersey/somePath/{**path**}" with pathVariable <br/>
map "/jersey/someGet?{**get**}" with GET method <br/>
map "/jersey/someCombined/{**path**}?{**get**}" with both pathVariable and GET method <br/>
map "/jersey/somePost" with POST method (form fields "**param1**" and "**param2**") <br/>
index.jsp with links to all above and <input> fields for POST <br/>